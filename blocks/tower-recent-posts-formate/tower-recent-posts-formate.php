<?php
/**
 * Team-Darstellung für Tower Productions 2022
 * Beruhend auf dem CPT "formate"
 * Dieser wird in der functions.php des themes registriert. (besser wäre hier, aber so geht es erstmal)
 */



$output = construct_html_formate();

function construct_html_formate() {
    $postlist = '';
                       
	// abrufen der genres
	// $genres = get_terms([
	// 	'taxonomy' => 'genre',
	// 	'hide_empty' => true,
	// ]);
	// foreach ($genres as $genre):

		// Auflistung der formate

		$formate_args = array (
			'post_type' => 'formate',
			'post_status' => array('publish'),
			'posts_per_page' => get_option( 'posts_per_page' ),
			// 'tax_query' => array(
			// 	array(
			// 		'taxonomy' => 'genre',
			// 		'field'    => 'slug',
			// 		'terms'    => $genre->slug,
			// 	),
			// )
		);

		$formate_query = new WP_Query($formate_args);

		if ( $formate_query->have_posts() ) : 
			$uniqueID = uniqid();

			$postlist .= '
				<div class="container-fluid max-width-true">
					<div class="row position-relative" orientation="horizontal">
						<div class="col-1 pr-0 col-md-1 pr-md-3">
							<button type="button" data-role="none" class="slick-left" aria-label="Previous" role="button">"Previous"</button>
						</div>
						<div class="col-10 col-md-10">
							<div class="slick slick-slider-'.$uniqueID.' d-flex no-shadow" id="slick-slider-'.$uniqueID.'" data-slick=\'{"slidesToShow": 4, "slidesToScroll": 1, "arrows": true}\'>'; ?>
								
								<?php while ( $formate_query->have_posts() ) : 
									$formate_query->the_post(); 
									$felder = get_fields();
									$postID = get_the_ID();
									?>
			<?php
			$postlist .= 			'<a href="'.get_the_permalink().'" class="slick-item format" style="height:auto;">'; ?>
										<?php
										/*
										if(!empty(catch_that_image())) {
											$image_id = attachment_url_to_postid(catch_that_image());
											if(!empty($image_id)) { 
												$url = wp_get_attachment_image_url($image_id, 'formate-slider'); 
											} else { 
												$url = catch_that_image(); 
											}
										
			$postlist .=				'<img src="'.$url.'" alt="Vorschaubild für das Sende-Format '.get_the_title().'" draggable="false" style="min-height:600px;" />';
										}*/
			$postlist .=				'<img src="'.get_the_post_thumbnail_url($postID).'" alt="Vorschaubild für das Sende-Format '.get_the_title().'" draggable="false" style="min-height:600px;" />';		
										?>
			<?php
			$postlist .= '
										<div class="format-beschreibung py-3 px-4">
											<div class="format-sender">'.get_field('sender').'</div>
											<h4 class="format-titel">'.get_the_title().'</h4>
										</div>
									</a>'; ?>

								<?php endwhile; ?>
			<?php
			$postlist .= '
							</div>
						</div>
						<div class="col-1 pl-0 col-md-1 pl-md-3">
							<button type="button" data-role="none" class="slick-right" aria-label="Next" role="button">"Next"</button>
						</div>
					</div>
				</div>'; ?>
		<?php
		endif;
		wp_reset_postdata();
	//endforeach; 

	$postlist .= "
	<script>
		const slider = document.querySelectorAll('.formate');
		let isDown = false;
		let startX;
		let scrollLeft;

		for(let i = 0; i<slider.length; i++){
			slider[i].addEventListener('mousedown', (event) => {
				isDown = true;
				event.target.classList.add('active');
				startX = event.pageX - slider[i].offsetLeft;
				scrollLeft = slider[i].scrollLeft;
			});
			slider[i].addEventListener('mouseleave', (event) => {
				isDown = false;
				event.target.classList.remove('active');
			});
			slider[i].addEventListener('mouseup', (event) => {
				isDown = false;
				event.target.classList.remove('active');
			});
			slider[i].addEventListener('mousemove', (event) => {
				if(!isDown) return;
				event.preventDefault();
				const x = event.pageX - slider[i].offsetLeft;
				const walk = (x - startX) * 3; //scroll-fast
				slider[i].scrollLeft = scrollLeft - walk;
				//console.log(walk);
			});
		}//endfor
	</script>
	";

    return $postlist;
}