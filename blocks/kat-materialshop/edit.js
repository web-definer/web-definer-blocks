/**
 * External dependencies
 */



/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';


/**
 * Internal dependencies
 */



export default function edit({ attributes }) {
	return (
		<>	
			<div className='wp-block-group'>
				<h4>Material-Shop</h4>
				<p>An dieser Stelle wird im Frontend der Materialshop angezeigt.</p>
			</div>
		</>
	);
};

