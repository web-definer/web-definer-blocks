/**
 * External dependencies
 */
 import classNames from 'classnames';


 /**
  * WordPress dependencies
  */
 import { __ } from '@wordpress/i18n';
 import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';
 
 
 /**
  * Internal dependencies
  */

 
 
 
 export default function edit({ attributes, setAttributes, className, clientId }) {
 
	const {
		linkText,
		blockId
	} = (attributes);

	React.useEffect( () => {
		if ( ! blockId ) {
			setAttributes( { blockId: clientId } );
		}
	}, [] );

	const blockProps = useBlockProps({
		className: classNames(className, 'read-more-toggle')
	});

	const innerBlocksProps = useInnerBlocksProps(
		blockProps,
		{
			templateLock: false,
			template: [ ["core/paragraph", {}] ],
			orientation:"horizontal"
		}
	);



	return (
		<>			
			<div { ...blockProps }>
				<h4>Weiterlesen-Toggle</h4>
				<TextControl
						label="Beschriftung"
						value={linkText}
						onChange={(linkText) => setAttributes({ linkText: linkText })}
				/>
					
	
				<div className='wp-block-group'>
					<h4>ausklappbarer Text:</h4>
					<div id={"ausklapptext"}  {...innerBlocksProps}></div>
				</div>
			</div>

		</>
	);
 };
 
 