/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */



export default function save( { attributes } ) {
    const { 
        linkText,
        blockId     
    } = (attributes);
   

	const blockProps = useBlockProps.save();

    const innerBlocksProps = useInnerBlocksProps.save( {
        orientation: "horizontal" 
    } );

    let htmlTemplate = '';

        htmlTemplate =  <div className={"collapse"} id={"collapse-"+blockId}>
                            <div {...innerBlocksProps}   />
                        </div>;
  

    return (
        <div { ...blockProps }>
            <a 
                data-bs-toggle={"collapse"} 
                href={"#collapse-"+ blockId} 
                role={"button"} 
                aria-expanded={"false"} 
                aria-controls={"collapse-"+ blockId}>
                {linkText}
            </a>

            {htmlTemplate}
        </div>
    );
}
