/**
 * External dependencies
 */


/**
 * WordPress dependencies
 */
 import { useBlockProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */



export default function save( { attributes } ) {
    const { 
        linkText,
        blockId,
        vorschauText,
        hauptText,
        nummer,
        ueberschrift  
    } = (attributes);
   

	const blockProps = useBlockProps.save();

   

    return (
        <div { ...blockProps }>
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-12 col-lg-6 position-relative'>
                        <div class="nummerierung">{nummer}</div>
                        <h3>{ueberschrift}</h3>
                    </div>
                    <div className='col-12 col-lg-6'>
                        <RichText.Content tagName="div" value={ vorschauText } className="vorschau-text" />

                        <a 
                            data-bs-toggle={"collapse"} 
                            href={"#collapse-"+ blockId} 
                            role={"button"} 
                            aria-expanded={"false"} 
                            aria-controls={"collapse-"+ blockId}
                            className={"hide-if-open text-end"}>
                            {linkText}
                        </a>

                        <RichText.Content tagName="div" value={ hauptText } className={"haupt-text" + " collapse"} id={ "collapse-"+ blockId } />
                    </div>
                </div>
            </div>
        </div>
    );
}
