/**
 * External dependencies
 */
 import classNames from 'classnames';


 /**
  * WordPress dependencies
  */
 import { __ } from '@wordpress/i18n';
 import { useBlockProps  } from '@wordpress/block-editor';
 
 
 /**
  * Internal dependencies
  */
import { RichText } from '@wordpress/block-editor';
 
 
 
 export default function edit({ attributes, setAttributes, className, clientId }) {
 
	const {
		linkText,
		blockId,
		vorschauText,
		hauptText,
        nummer,
        ueberschrift 
	} = (attributes);

	if ( blockId != clientId) {
		setAttributes( { blockId: clientId } );
	}


	const blockProps = useBlockProps({
		className: classNames(className, 'read-more-toggle')
	});

	



	return (
		<>			
			<div { ...blockProps }>
				<h4>Weiterlesen-Toggle</h4>
				<TextControl
						label="Nummerierung"
						value={nummer}
						onChange={(nummer) => setAttributes({ nummer: nummer })}
						placeholder="1"
				/>

				<TextControl
						label="Überschrift"
						value={ueberschrift}
						onChange={(ueberschrift) => setAttributes({ ueberschrift: ueberschrift })}
						placeholder="Überschrift..."
				/>
					
	
				<div className='wp-block-group'>
					<h4>Vorschau-Text:</h4>
					<RichText
						tagName="div"
						className="vorschau-text"
						value={ vorschauText }
						onChange={ ( vorschauText ) => setAttributes( { vorschauText: vorschauText } ) }
						placeholder={ __( 'Hier den ersten direkt sichtbaren Teil des Textes...' ) }
					/>
				</div>

				<TextControl
						label="Text des öffnen-lLinks"
						value={linkText}
						onChange={(linkText) => setAttributes({ linkText: linkText })}
				/>

				<div className='wp-block-group'>
					<h4>Haupt-Text:</h4>
					<RichText
						tagName="div"
						className="haupt-text"
						value={ hauptText }
						onChange={ ( hauptText ) => setAttributes( { hauptText: hauptText } ) }
						placeholder={ __( 'Hier den langen Haupttext...' ) } 
					/>
				</div>

			</div>

		</>
	);
 };
 
 