/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */
// für Color-Auswahl
import { getColorClassName } from '@wordpress/block-editor';


export default function save( { attributes } ) {
    const { 
        maxWidth,
        layout: {
			justifyContent = 'left',
			orientation = 'horizontal',
			flexWrap = 'wrap',
		} = {},       
    } = (attributes);
   
    let maxWidthClass;
    if(maxWidth === true) {
        maxWidthClass = 'max-width-true';
    } else {
        maxWidthClass = '';
    }

	const blockProps = useBlockProps.save();

    const innerBlocksProps = useInnerBlocksProps.save( {
        className: classNames('row'),
        orientation: orientation 
    } );

    return (
        <div { ...blockProps }>
            <div className={'container-fluid ' + maxWidthClass}>
                <div {...innerBlocksProps}   />
            </div>
        </div>
    );
}
