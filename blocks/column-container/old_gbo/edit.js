/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';

// für die Sidebar (Block-Settings)
import { InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow } from '@wordpress/components';

// für Toggle
import { CheckboxControl } from '@wordpress/components';
import { useState } from '@wordpress/element';

/**
 * Internal dependencies
 */


var ALLOWED_BLOCKS_BS_CONTAINER = [
	'web-definer/column-1',
	'web-definer/column-2',
	'web-definer/column-3',
	'web-definer/column-4',
	'web-definer/column-5',
	'web-definer/column-6',
	'web-definer/column-7',
	'web-definer/column-8',
	'web-definer/column-9',
	'web-definer/column-10',
	'web-definer/column-11',
	'web-definer/column-12'
];


export default function edit({ attributes, setAttributes, className }) {

	const {
		maxWidth,
		layout: {
			justifyContent = 'left',
			orientation = 'horizontal',
			flexWrap = 'wrap',
		} = {},
	} = (attributes);

	const blockProps = useBlockProps({
		className: classNames(className, {
				'justify-content-right': justifyContent === 'right',
				'justify-content-space-between': justifyContent === 'space-between',
				'justify-content-left': justifyContent === 'left',
				'justify-content-center': justifyContent === 'center',
				'is-vertical': orientation === 'vertical',
				'no-wrap': flexWrap === 'nowrap',
		}),
	});

	const innerBlocksProps = useInnerBlocksProps(
        blockProps,
        {
			allowedBlocks: ALLOWED_BLOCKS_BS_CONTAINER,
			templateLock: false,
			orientation:"horizontal"	
		}
    );


	const MaxWidthCheckboxControl = () => {
		const { isChecked, setChecked } = useState(true);
		return (
			<CheckboxControl
				label="begrenzte Breite"
				help="Container-Breite begrenzen?"
				checked={maxWidth}
				onChange={(value) => {
					setChecked;
					setAttributes({ maxWidth: value });
				}}
			/>
		);
	};



	return (
		<>		

				<div {...innerBlocksProps} />
				
				<InspectorControls>
					<PanelBody
						title={__('Einstellungen für Container')}
						initialOpen={true}
					>
						<PanelRow>
							<MaxWidthCheckboxControl />
						</PanelRow>
						
					</PanelBody>
				</InspectorControls>

		</>
	);
};

