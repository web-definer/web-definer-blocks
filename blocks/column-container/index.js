/**
 * WordPress dependencies
 */
import { registerBlockType } from '@wordpress/blocks';


/**
 * Internal dependencies
 */
import metadata from './block.json';
import edit from './edit';
import save from './save';
import deprecated from './deprecated';

// Destructure the json file to get the name of the block
const { name } = metadata;


registerBlockType( name, {
	edit,
	save,
	deprecated,
} );