/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';

// für die Sidebar (Block-Settings)
import { InspectorControls } from '@wordpress/block-editor';
import { Panel, PanelBody, PanelRow, Dashicon } from '@wordpress/components';
import { more } from '@wordpress/icons';


// für Toggle
import { CheckboxControl } from '@wordpress/components';
import { useState } from '@wordpress/element';

// für Select
import { SelectControl } from '@wordpress/components';
//import { useState } from '@wordpress/element'; // schon da im toggle


/**
 * Internal dependencies
 */


var ALLOWED_BLOCKS_BS_CONTAINER = [
	'web-definer/column-1',
	'web-definer/column-2',
	'web-definer/column-3',
	'web-definer/column-4',
	'web-definer/column-5',
	'web-definer/column-6',
	'web-definer/column-7',
	'web-definer/column-8',
	'web-definer/column-9',
	'web-definer/column-10',
	'web-definer/column-11',
	'web-definer/column-12'
];


export default function edit({ attributes, setAttributes, className, clientId }) {

	const {
		backgroundColor,
		maxWidth,
		xsBlock,
		mdBlock,
		lgBlock,
		layout: {
			justifyContent = 'center',
			orientation = 'horizontal',
			flexWrap = 'wrap',
		} = {},
	} = (attributes);

	const blockProps = useBlockProps({
		className: classNames(className, {
				'justify-content-right': justifyContent === 'right',
				'justify-content-space-between': justifyContent === 'space-between',
				'justify-content-left': justifyContent === 'left',
				'justify-content-center': justifyContent === 'center',
				'is-vertical': orientation === 'vertical',
				'no-wrap': flexWrap === 'nowrap',
		}),
	});

	const innerBlocksProps = useInnerBlocksProps(
        blockProps,
        {
			allowedBlocks: ALLOWED_BLOCKS_BS_CONTAINER,
			template: [ [ 'web-definer/column-12', {} ] ],
			templateLock: false,
			orientation:"horizontal"	
		}
    );


	const MaxWidthCheckboxControl = () => {
		const { isChecked, setChecked } = useState(true);
		return (
			<CheckboxControl
				label="begrenzte Breite"
				help="Container-Breite begrenzen?"
				checked={maxWidth}
				onChange={(value) => {
					setChecked;
					setAttributes({ maxWidth: value });
				}}
			/>
		);
	};

	const MyBackgroundColorControl = () => {
		const [ backgroundColorState, setBackgroundColor ] = useState( '' );
	
		return (
			<SelectControl
				label="Hintergrund"
				value={ backgroundColor }
				options={ [
					{ label: 'kein', value: '' },
					{ label: 'Tower-Schwarz', value: 'bg-towerschwarz' },
					{ label: 'Orange', value: 'bg-orange' },
					// { label: 'Arcus Schwarz', value: 'bg-arcusschwarz' },
					// { label: 'Türkies', value: 'bg-tuerkies' },
					//{ label: 'Hellblau', value: 'bg-hellblau' },
					//{ label: 'Rot', value: 'bg-rot' },
					//{ label: 'Gelb', value: 'bg-gelb' },
					{ label: 'Weiß', value: 'bg-weiss' },
					{ label: 'Lila', value: 'bg-lila' },
					//{ label: 'Grau', value: 'bg-grau' },
					//{ label: 'Sinus-Schwarz', value: 'bg-sinusschwarz' },
				] }
				onChange={ ( newBackgroundColor ) => {
					setBackgroundColor( backgroundColorState );
					setAttributes({ backgroundColor: newBackgroundColor });
				}}
				__nextHasNoMarginBottom
			/>
		);
	};

	const MyResponsiveControl = () => {
		const { isChecked, setChecked } = useState(true);
		return (
			<>	
				<PanelRow>
					<div>
						<Dashicon icon="smartphone" />
						<CheckboxControl
							label="Mobil"
							help="Auf Mobil-Geräten anzeigen?"
							checked={xsBlock}
							onChange={(value) => {
								setChecked;
								setAttributes({ xsBlock: value });
							}}
						/>
					</div>
				</PanelRow>
				<PanelRow>
					<div>
						<Dashicon icon="tablet" />
						<CheckboxControl
							label="Tablet"
							help="Auf Tablet anzeigen? (ab 768px aufwärts)"
							checked={mdBlock}
							onChange={(value) => {
								setChecked;
								setAttributes({ mdBlock: value });
							}}
						/>
					</div>
				</PanelRow>
				<PanelRow>
					<div>
						<Dashicon icon="desktop" />
						<CheckboxControl
							label="Desktop"
							help="Auf Desktop anzeigen? (ab 992px aufwärts)"
							checked={lgBlock}
							onChange={(value) => {
								setChecked;
								setAttributes({ lgBlock: value });
							}}
						/>
					</div>
				</PanelRow>

			</>
		);
	}


	return (
		<>		
			
			<Panel>
				<PanelBody title="Container (Zeile für mehrer Spalten)" icon={ more } initialOpen={ true }>
					<PanelRow><div {...innerBlocksProps} /></PanelRow>
				</PanelBody>
			</Panel>
			
			{/*
			<div class='edit-post-meta-boxes-area'>
				<div id={ 'webdef-container-' + clientId } class="postbox">
					<div class="postbox-header">
						<h2 class="hndle ui-sortable-handle">Container (Zeile für mehrer Spalten)</h2>
						<div class="handle-actions hide-if-no-js">
							<button type="button" class="handlediv" aria-expanded="true">
								<span class="screen-reader-text">Bedienfeld umschalten: Container</span>
								<span class="toggle-indicator" aria-hidden="true"></span>
							</button>
						</div>
					</div>
					<div class="inside -top">
						<div class="webdef-container">
							<div {...innerBlocksProps} />
						</div>
					</div>
				</div>
			</div>
			*/ }

			<InspectorControls>
				<Panel>
					<PanelBody
						title={__('Einstellungen für Container')}
						initialOpen={true}
					>
						<PanelRow>
							<MaxWidthCheckboxControl />
						</PanelRow>

						<PanelRow>
							<MyBackgroundColorControl />
						</PanelRow>


						<MyResponsiveControl />
						

					</PanelBody>
				</Panel>
			</InspectorControls>

		</>
	);
};

