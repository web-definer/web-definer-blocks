/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */
// für Color-Auswahl
import { getColorClassName } from '@wordpress/block-editor';


export default [
    {
        attributes: {
            maxWidth: {
				type: 'boolean',
			},
        },
        save( { attributes } ) {
            const { 
                maxWidth         
            } = (attributes);
        

            var maxWidthClass = maxWidth === true ? 'max-width-true' : '';

            const blockProps = useBlockProps.save();

            const innerBlocksProps = useInnerBlocksProps.save( {
                className: classNames('row', 'w-100'),
                orientation: "horizontal" 
            } );

            return (
                <div { ...blockProps }>
                    <div className={'container-fluid ' + maxWidthClass}>
                        <div {...innerBlocksProps}   />
                    </div>
                </div>
            );
        }
    }
]