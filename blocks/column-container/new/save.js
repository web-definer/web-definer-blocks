/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */
// für Color-Auswahl
import { getColorClassName } from '@wordpress/block-editor';


export default function save( { attributes } ) {
    const { 
        backgroundColor,
        maxWidth,
        xsBlock,
		mdBlock,
		lgBlock,
        layout: {
			justifyContent = 'left',
			orientation = 'horizontal',
			flexWrap = 'wrap',
		} = {},       
    } = (attributes);
   
    let maxWidthClass;
    if(maxWidth === true) {
        maxWidthClass = 'max-width-true';
    } else {
        maxWidthClass = '';
    }

    let xsBlockClass;
    if(xsBlock === true) {
        xsBlockClass = ' ';
    } else {
        xsBlockClass = ' d-none';
    }

    let mdBlockClass;
    if(mdBlock === true) {
        mdBlockClass = ' d-md-block';
    } else {
        mdBlockClass = ' d-md-none';
    }

    let lgBlockClass;
    if(lgBlock === true) {
        lgBlockClass = ' d-lg-block';
    } else {
        lgBlockClass = ' d-lg-none';
    }


	const blockProps = useBlockProps.save( {
        className: classNames(backgroundColor)
    });

    const innerBlocksProps = useInnerBlocksProps.save( {
        className: classNames('row'),
        orientation: orientation 
    } );

    return (
        <div { ...blockProps }>
            <div className={'container-fluid ' + maxWidthClass + xsBlockClass + mdBlockClass + lgBlockClass}>
                <div {...innerBlocksProps}   />
            </div>
        </div>
    );
}
