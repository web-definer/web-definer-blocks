/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps, RichText } from '@wordpress/block-editor';

import { URLInputButton } from '@wordpress/block-editor';

// für die Sidebar (Block-Settings)
import { InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow } from '@wordpress/components';

// für Toggle
import { CheckboxControl } from '@wordpress/components';
import { useState } from '@wordpress/element';

// für ein Auswahlmenü
import { SelectControl } from '@wordpress/components';
import { withState }  from '@wordpress/compose';

/**
 * Internal dependencies
 */





export default function edit({ attributes, setAttributes, className }) {

	const { btn_text, ctaurl, btn_type, targetBlank } = (attributes);

	const TargetBlankCheckboxControl = () => {
		const { isChecked, setChecked } = useState(true);
		return (
			<CheckboxControl
				label="neuer Tab"
				help="Link in neuem Tab/Fenster öffnen?"
				checked={targetBlank}
				onChange={(value) => {
					setChecked;
					setAttributes({ targetBlank: value })
				}}
			/>
		)
	};

	const blockProps = useBlockProps({
		className: className
	});


	return (
		<>
			<div { ...blockProps }>
				<h4>Button</h4>
				<div className={"row"}>

					<div className={className}>
						<RichText
							key="btn_text"
							tagName="strong"
							onChange={(content) => setAttributes({ btn_text: content })}
							value={btn_text}
							placeholder={__('Button-Text')}
						/>
						<URLInputButton
							className={className}
							url={ctaurl}
							onChange={(url, post) => setAttributes({ ctaurl: url, ctabutton: (post && post.title) || 'Link auswählen' })}
						/>
					</div>
				</div>
			</div>

			<InspectorControls>
				<PanelBody
					title={__('Einstellungen für Button')}
					initialOpen={true}
				>
					<SelectControl
						label="Button-Typ"
						value={btn_type}
						options={[
							{ label: 'Primär', value: 'btn-primary' },
							{ label: 'Sekundär', value: 'btn-secondary' },
							{ label: 'Info', value: 'btn-info' },
							{ label: 'Download', value: 'btn-download' },
						]}
						onChange={(value) => setAttributes({ btn_type: value })}
					/>
					<PanelRow>
						<TargetBlankCheckboxControl />
					</PanelRow>
				</PanelBody>
			</InspectorControls>

		</>
	);
};

