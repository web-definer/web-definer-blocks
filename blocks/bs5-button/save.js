/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */


export default function save( { attributes, className } ) {

    const { ctaurl, btn_text, btn_type, targetBlank } = (attributes);
    let linkTarget;

    const blockProps = useBlockProps.save();

    if (targetBlank == true) { linkTarget = '_blank'; } else { linkTarget = ''; }


    return (
        <div { ...blockProps }>
            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-12 " + className}>
                        <a className={"btn " + btn_type} href={ctaurl} target={linkTarget} rel={"noopener"}><span>{btn_text}</span></a>
                    </div>
                </div>
            </div>
        </div>
    );
}
