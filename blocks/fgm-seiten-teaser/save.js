/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */
// für Color-Auswahl
import { getColorClassName } from '@wordpress/block-editor';


export default function save( { attributes, className } ) {
   

	const blockProps = useBlockProps.save();

    const innerBlocksProps = useInnerBlocksProps.save( {
        className: classNames(className, 'seitenteaser'),
        orientation: "vertical" 
    } );

    return (
        <div { ...blockProps }>
            <div {...innerBlocksProps}   />
        </div>
    );
}
