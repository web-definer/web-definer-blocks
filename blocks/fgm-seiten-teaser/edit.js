/**
 * External dependencies
 */
import classNames from 'classnames';

/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';

// für die Sidebar (Block-Settings)
import { InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow } from '@wordpress/components';

// für Toggle
import { CheckboxControl } from '@wordpress/components';
import { useState } from '@wordpress/element';

/**
 * Internal dependencies
 */


const ALLOWED_BLOCKS_FGM_SEITENTEASER = [
	'core/image',
	'core/paragraph',
	'core/heading',
	'mehne/btn-primary',
];

const BLOCKS_TEMPLATE_FGM_SEITENTEASER = [
    [ 'core/image', {} ],
    [ 'core/heading', { placeholder: 'Teaser-Überschrift' } ],
    [ 'core/paragraph', { placeholder: 'kurzer Tesertext' } ],
    [ 'mehne/btn-primary', {} ],
];


export default function edit({ className }) {

	

	const blockProps = useBlockProps({
		className: classNames(className, 'seitenteaser'),
	});

	const innerBlocksProps = useInnerBlocksProps(
        blockProps,
        {
			allowedBlocks: ALLOWED_BLOCKS_FGM_SEITENTEASER,
			template: BLOCKS_TEMPLATE_FGM_SEITENTEASER,
			templateLock: true,
			orientation:"vartical"
		}
    );

	return (
		<>			
			<div {...innerBlocksProps} />

			<InspectorControls>
				<PanelBody
					title={__('Einstellungen für Teaser')}
					initialOpen={true}
				>
					<PanelRow>
						keine Besonderheiten
					</PanelRow>
					
				</PanelBody>
			</InspectorControls>
			
		</>
	);
};

