/**
 * External dependencies
 */
import classNames from 'classnames';


/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';


/**
 * Internal dependencies
 */



export default function edit({ attributes, className }) {

	const {
		desktopImage,
		mobileImage
	} = (attributes);

	const blockProps = useBlockProps({
		className: classNames(className, 'intro-image'),
	});

	const innerBlocksProps = useInnerBlocksProps(
        blockProps,
        {
			template: [ [ 'core/image', {className: 'd-lg-none'} ], [ 'core/image', {className: 'd-none d-lg-block'} ] ],
			templateLock: 'all'
		}
    );


	return (
		<>			
			<div { ...blockProps }>
				<h4>Erstes Bild = Mobil + zweites Bild = Desktop</h4>
				<div {...innerBlocksProps} />
			</div>
		</>
	);
};

