/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */



export default function save( { attributes } ) {
    const { 
        desktopImage,
		mobileImage     
    } = (attributes);
   

	const blockProps = useBlockProps.save();

    const innerBlocksProps = useInnerBlocksProps.save();

    return (
        <div { ...blockProps }>
            <div {...innerBlocksProps} />
            <img id="intro-image-claim" src="/wp-content/themes/erzieher-bw-22/images/svg/Claim_RGB.svg" alt="Claim der Kampagne: 'mehr bekommst du nirgendwo'" />
        </div>
    );
}
