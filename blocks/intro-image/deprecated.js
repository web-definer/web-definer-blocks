/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */



export default [
    {
        attributes: {
            linkText: {
				type: 'string',
			},
            blockId: {
              type: "string"
            }
        },
        save( { attributes } ) {
            const { 
                linkText,
                blockId       
            } = (attributes);
           
        
            const blockProps = useBlockProps.save();
        
            const innerBlocksProps = useInnerBlocksProps.save( {
                orientation: "horizontal" 
            } );
        
            return (
                <div { ...blockProps }>
                    <a 
                        dataBsToggle={"collapse"} 
                        href={"#collapse-"+ blockId} 
                        role={"button"} 
                        ariaExpanded={"false"} 
                        ariaControls={"collapseExample"}>
                        {linkText}
                    </a>
        
                    <div className={"collapse"} id={"collapse-"+blockId}>
                        <div {...innerBlocksProps}   />
                    </div>
                </div>
            );
        }
    }
]