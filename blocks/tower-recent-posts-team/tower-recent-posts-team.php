<?php
/**
 * Team-Darstellung für Tower Productions 2022
 * Beruhend auf dem CPT "team"
 * Dieser wird in der functions.php des themes registriert. (besser wäre hier, aber so geht es erstmal)
 */



$output = construct_html();

function construct_html() {
    $postlist = '';
                       
	$cpt_args = array(
		'post_type' => 'team',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'DESC'
	);
	
	$cpt_query = new WP_Query($cpt_args);

	if($cpt_query->have_posts()) :
		$postlist .= '<div class="container-fluid">
						<div class="row teammitglieder">';
							while($cpt_query->have_posts()): $cpt_query->the_post();
								$image = get_field('bild');
								$email = get_field('email');
								$position = get_field('position');
								ob_start(); ?>
								<div class="col-12 col-sm-6 col-lg-3 mb35 teammitglied">
									<div class="row h-100 slid-out justify-content-center">
										<div class="col">
											<div class="h-100 bg-towerschwarz">
											<?php if ($email != '') { echo '<a href="mailto:'.$email.'">'; } ?>
												<div class="col-12 mb10">
													<img src="<?=$image['sizes']['main-image-small']?>" alt="<?=$image['alt']?>" class="w-100" />
												</div>
												<div class="col-12 py-3 px-5">
													<h3><?=get_the_title()?></h3>
													<?php if ($position != '') { ?>
														<div style="color:var(--wp--preset--color--orange);"><?=$position?></div>
													<?php } ?>
													<?php
													/*
													if ($email != '') { ?>
														<a href="mailto:<?=$email?>"><?=$email?></a>
													<?php 
													} */
													?>
												</div>
											<?php if ($email != '') { echo "</a>"; } ?>
														
											</div>
										</div>
									</div>
								</div>
								<?php 
								$postlist .= ob_get_clean();
							endwhile;
		$postlist .= '	</div>
					</div>';
	endif;
	wp_reset_postdata();


    return $postlist;
}