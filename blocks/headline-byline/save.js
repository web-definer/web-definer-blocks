/**
 * External dependencies
 */
 import classNames from 'classnames';

/**
 * WordPress dependencies
 */
 import { useBlockProps, useInnerBlocksProps } from '@wordpress/block-editor';
 
/**
 * Internal dependencies
 */



export default function save( { attributes } ) {
    const { 
        headline,
        byline       
    } = (attributes);
   

	const blockProps = useBlockProps.save();


    return (
        <div { ...blockProps }>
            <h1 className='text-center'>{headline}<br /><span className='byline'>{byline}</span></h1>
        </div>
    );
}
