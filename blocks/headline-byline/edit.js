/**
 * External dependencies
 */
import classNames from 'classnames';


/**
 * WordPress dependencies
 */
import { __ } from '@wordpress/i18n';
import { useBlockProps, useInnerBlocksProps  } from '@wordpress/block-editor';


/**
 * Internal dependencies
 */



export default function edit({ attributes, setAttributes, className }) {

	const {
		headline,
		byline
	} = (attributes);

	const blockProps = useBlockProps({
		className: classNames(className, 'seitenteaser'),
	});


	return (
		<>			
			<div { ...blockProps }>
				<TextControl
					label="Headline"
					value={headline}
					onChange={(headline) => setAttributes({ headline: headline })}
				/>
					
				<TextControl
					label="Byline"
					value={byline}
					onChange={(byline) => setAttributes({ byline: byline })}
				/>

			</div>
		</>
	);
};

