<?php
/**
 * projekte-Darstellung für Annette Kolibar
 * Beruhend auf dem CPT "projekte"
 * Dieser wird in der functions.php des themes registriert. (aus Faulheit)
 */



$output = construct_html();

function construct_html() {
    $postlist = '';

    $all_cats = get_terms( array(
        'taxonomy' => 'category',
        'hide_empty' => false,
    ) );
/*
	$postlist .= '<div class="container-fluid">
					<div class="row">
						<div class="col-12 col-lg-5">
							<h1 name="projekte" id="projekte">Projekte</h1>
						</div>';
*/
	$postlist .= '
				<div style="background-image:url('.get_stylesheet_directory_uri().'/images/hintergrund_beton_2000x3000px.png);background-size:auto auto; background-repeat:repeat;">
					<div class="container-fluid pt45">
						<div class="row">

							<div class="col-12">
								<div class="isotope-button-group filter-button-group">
									<button data-filter="*" class="btn btn-info is-checked">Projekte</button>';
									foreach ($all_cats as $key => $value) {
										ob_start();
										?>
											<button class="btn btn-info" data-filter=".<?=$value->slug?>"><?=$value->name?></button>
										<?php
										$postlist .= ob_get_clean();
									}
									
    $postlist .= '				</div>
							</div>
						</div>
					</div>';

	$postlist .= '
					<div class="row justify-content-end pe-3">
						<div class="col-12 col-lg-3 text-end pe-5">
							<a href="/leistungen">Unsere Leistungen <img src="'.get_stylesheet_directory_uri().'/images/pfeil_kurz.svg" alt="Pfeil nach Rechts." style="display:inline-block;width:67px;height:48px;" /></a>
						</div>
					</div>
				';


    $postlist .=    '<div class="container-fluid isotope-container"><div class="row isotope-grid">';
                       
						$cpt_args = array(
							'post_type' => 'projekte',
							'posts_per_page' => -1,
							'orderby' => 'menu_order'
						);
						
						$cpt_query = new WP_Query($cpt_args);

                        if($cpt_query->have_posts()) :
                            while ($cpt_query->have_posts()) : $cpt_query->the_post();
								$post_cats = get_the_terms($post->ID, 'category');
								$post_cats_string = join(' ', wp_list_pluck($post_cats, 'slug'));

								ob_start(); ?>
                                	<div class="col-12 col-lg-4 isotope-grid-item <?=$post_cats_string?>">
										<a href="<?php the_permalink(); ?>">
											<figure>
												<?php the_post_thumbnail('full', ['class' => 'd-block']); ?>
												<figcaption>
													<span class="categories">
													<?php
														$post_cats = wp_get_post_categories(get_the_ID(), array('fields'=>'names'));
														foreach ($post_cats as $key => $value) {
															if ($key === array_key_last($post_cats)) {
																echo $value;
															} else {
																echo $value . ', ';
															}
															
														}
													?>
													</span></br>
													<span><?php echo get_the_title(); ?></span>
												</figcaption>
											</figure>
										</a>
									</div>
								<?php
								$postlist .= ob_get_clean();
                            endwhile;
                        endif;
                        wp_reset_postdata();
	$postlist .= '	</div>';
	$postlist .= '</div></div>';

    return $postlist;
}