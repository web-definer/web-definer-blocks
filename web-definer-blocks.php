<?php
/**
 * Plugin Name:       web-definer Blocks Plugin
 * Description:       Soll alle Gutenberg Blöcke zu Verfügung stellen, die mit dem Theme nötig sind.
 * Requires at least: 5.9.1
 * Requires PHP:      7.4
 * Version:           0.2.1
 * Author:            Kai Meinhardt
 * Author URI:		  https://web-definer.de
 * License:           MIT
 * Text Domain:       web-definer
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/tutorials/block-tutorial/writing-your-first-block-type/
 */
function create_block_multiple_blocks_plugin_block_init() {

	// register all blocks here
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/column-container/' );
	//
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/bs5-button/' );
	//
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/read-more-toggle-with-preview/' );
	//
	//register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/fgm-seiten-teaser/' );
	//
	//register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/headline-byline/' );

	// if it is a block with serverside rendering do it this way
	//register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/kolibar-projekte-masonry/', array('render_callback' => 'kolibar_projekte_masonry') );
	
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/tower-recent-posts-team/', array('render_callback' => 'tower_recent_posts_team') );

	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/tower-recent-posts-formate/', array('render_callback' => 'tower_recent_posts_formate') );
	
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/intro-image/');
}
add_action( 'init', 'create_block_multiple_blocks_plugin_block_init' );

// use a callback for serverside rendering
function kolibar_projekte_masonry() {
	$file = dirname(__FILE__) . '/blocks/kolibar-projekte-masonry/kolibar-projekte-masonry.php';
	if(file_exists($file)) {
		require_once($file);
		return $output;
	} else {
		return 'Konnte Quellcode-Datei für die Ausgabe der Informationen nicht finden. (file_exists() is false)';
	}
}

function tower_recent_posts_team() {
	$file = dirname(__FILE__) . '/blocks/tower-recent-posts-team/tower-recent-posts-team.php';
	if(file_exists($file)) {
		$output = ''; //reset
		require_once($file);
		return $output;
	} else {
		return 'Konnte Quellcode-Datei für die Ausgabe der Informationen nicht finden. (file_exists() is false)';
	}
}

function tower_recent_posts_formate() {
	$file = dirname(__FILE__) . '/blocks/tower-recent-posts-formate/tower-recent-posts-formate.php';
	if(file_exists($file)) {
		$output = ''; //reset
		require_once($file);
		return $output;
	} else {
		return 'Konnte Quellcode-Datei für die Ausgabe der Informationen nicht finden. (file_exists() is false)';
	}
}